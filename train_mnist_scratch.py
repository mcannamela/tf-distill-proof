# deprecated in favor of train_mnist_teacher; it's kept around just to show people how I refactor
import argparse
import os

import tensorflow as tf
from tensorflow.contrib import layers

from mnist_data import load_mnist

parser = argparse.ArgumentParser(description='Train a conv net on mnist')
parser.add_argument('savepath', help='Path for us to save all the various output.')
parser.add_argument('--restore', action='store_true', default=False, help='If present, restore from the last available checkpoint')


def remove_savepath_if_necessary(path):
    if os.path.exists(path):
        tf.gfile.DeleteRecursively(path)


def make_savepath_if_necessary(path):
    if not os.path.exists(path):
        os.mkdir(path)

args = parser.parse_args()
print("Output path is {}".format(args.savepath))
remove_savepath_if_necessary(args.savepath)
make_savepath_if_necessary(args.savepath)

print("Output path ok! Begin example.")

graph = tf.Graph()
sess = tf.Session(graph=graph)
with graph.as_default():

    input_pl = tf.placeholder(tf.float32, shape=[None, 784], name='input_pl')
    input_image = tf.reshape(input_pl, [-1, 28, 28, 1])

    one_hot_labels_pl = tf.placeholder(tf.float32, shape=[None, 10], name='labels_pl')

    is_training = tf.placeholder(tf.bool, name='is_training_pl')

    conv1 = layers.convolution2d(
        input_image,
        32,
        5,
        activation_fn=tf.nn.relu,
        biases_initializer=tf.constant_initializer(.1),
    )

    max_pool_1 = layers.max_pool2d(conv1, 2, 2)

    conv2 = layers.convolution2d(
        max_pool_1,
        64,
        5,
        activation_fn=tf.nn.relu,
        biases_initializer=tf.constant_initializer(.1)
    )

    max_pool_2 = layers.max_pool2d(conv2, 2, 2)

    n_elements_per_example = 7*7*64

    fc = layers.fully_connected(
        layers.flatten(max_pool_2),
        n_elements_per_example,
        activation_fn=tf.nn.relu,
        biases_initializer=tf.constant_initializer(.1)
    )

    drop = layers.dropout(
        fc,
        keep_prob=.5,
        is_training=is_training
    )

    logits_output = layers.fully_connected(
        layers.flatten(drop),
        10,
        scope='logits_output'
    )

    sigmoid_output = tf.nn.sigmoid(logits_output)

    xentropy = tf.losses.softmax_cross_entropy(one_hot_labels_pl, logits_output)

    optimizer = tf.train.AdamOptimizer(learning_rate=2e-5, beta1=0.99, beta2=0.9999, epsilon=1e-7)
    train_step = optimizer.minimize(xentropy)
    true_label = tf.argmax(one_hot_labels_pl, axis=1)
    predicted_label = tf.argmax(logits_output, axis=1)
    correct_prediction = tf.equal(predicted_label, true_label)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    test_accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    summ_train_acc = tf.summary.scalar('training_accuracy', accuracy)
    summ_test_acc = tf.summary.scalar('test_accuracy', test_accuracy)
    summ_all = tf.summary.merge_all()

    saver = tf.train.Saver()
    if args.restore:
        print("Restoring run from latest checkpoint")
        latest_checkpoint = tf.train.latest_checkpoint(args.savepath)
        saver.restore(sess, latest_checkpoint)

    summary_writer = tf.summary.FileWriter(args.savepath, graph)

    sess.run(tf.global_variables_initializer())

    saver.save(sess, os.path.join(args.savepath, "model.ckpt"), global_step=0)

    mnist = load_mnist()
    test_feed_dict = {
                input_pl: mnist.test.images, one_hot_labels_pl: mnist.test.labels, is_training: False
            }
    for i in range(30000):
        batch = mnist.train.next_batch(50)
        feed_dict = {input_pl: batch[0], one_hot_labels_pl: batch[1], is_training: True}
        sess.run(train_step, feed_dict=feed_dict)
        if i % 100 == 0:
            feed_dict[is_training] = False
            this_train_acc, summ = sess.run([accuracy, summ_train_acc], feed_dict=feed_dict)
            summary_writer.add_summary(summ, i)
            summary_writer.flush()
            print("step %d, training accuracy %g" % (i, this_train_acc))

        if i % 1000 == 0:
            this_test_acc, summ = sess.run([test_accuracy, summ_test_acc], feed_dict=test_feed_dict)
            summary_writer.add_summary(summ, i)
            summary_writer.flush()
            print("test accuracy %g" % this_test_acc)

    print("test accuracy %g" % sess.run(accuracy, feed_dict=test_feed_dict))
