import os

import tensorflow as tf


def remove_savepath_if_necessary(path):
    if os.path.exists(path):
        tf.gfile.DeleteRecursively(path)


def make_savepath_if_necessary(path):
    if not os.path.exists(path):
        os.mkdir(path)