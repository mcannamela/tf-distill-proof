import tensorflow as tf

from argument_parser import get_parser
from mnist_data import load_mnist
from shared import remove_savepath_if_necessary, make_savepath_if_necessary
from teacher_learner import Teacher

parser = get_parser(description="Train a convolutional teacher network on MNIST")

if __name__ == "__main__":
    args = parser.parse_args()
    print("Output path is {}".format(args.savepath))
    remove_savepath_if_necessary(args.savepath)
    make_savepath_if_necessary(args.savepath)

    graph = tf.Graph()
    sess = tf.Session(graph=graph)
    with graph.as_default():
        teacher = Teacher(
            sess, args.savepath, load_mnist(),
            n_filters=(32, 64), kernel_sizes=(5, 5)
        )

        teacher.train(
            n_training_steps=30000,
            n_batch=50,
            train_summary_interval=100,
            test_summary_interval=1000
        )