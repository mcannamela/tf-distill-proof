import argparse


def get_parser(description='Train a conv net on mnist'):
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('savepath', help='Path for us to save all the various output.')
    return parser