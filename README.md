# Reproductions of published work on neural net distillation

See Hinton, Geoffrey, Oriol Vinyals, and Jeff Dean. "Distilling the Knowledge in a Neural Network." stat 1050 (2015): 9.
APA	


## Why?
This repo should prove we can distill an MNIST neural net according to the published procedures.

## Quickstart
Build a test environment and run some tests

    ./build_env
    source ./activate
    pytest


## How to proceed

First, train a teacher model using train_mnist_teacher.py; subsequent training of learner models
depends on having one already trained.

Then, you can train learner models using train_mnist_learner.py

Use tensorboard to compare runs, and experiment with different distillation temperatures, network sizes, and weighting
between the data cross entropy term and the teacher term.
