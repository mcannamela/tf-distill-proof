import os

import tensorflow as tf

from argument_parser import get_parser
from mnist_data import load_mnist
from shared import remove_savepath_if_necessary, make_savepath_if_necessary
from teacher_learner import Learner, RestoredTeacher

parser = get_parser(description="Distill a teacher network into a learner using the method of Hinton.")
parser.add_argument('teacherpath', help='Path from which to load the teacher model.')
parser.add_argument('--label-weight', default=1.0, type=float, help='Weight given to the known label; teacher weight is (1.0 - label_weight)')
parser.add_argument('--n-hidden', default=200, type=int, help='Number of hidden units in each layer of the learner model')
parser.add_argument('--learning-rate', default=1e-4, type=float, help='Learning rate for the learner model.')
parser.add_argument('--temperature', default=1.0, type=float, help='Temperature of the transfer. See Hinton 2015, https://www.cs.toronto.edu/~hinton/absps/distillation.pdf')
parser.add_argument('--zero-mean-logits', action='store_true', default=False, help='Whether the teacher logits should be made zero-mean')


if __name__ == "__main__":
    args = parser.parse_args()
    runname = '_'.join([
        'n-hidden={}'.format(args.n_hidden),
        'label-weight={:.3e}'.format(args.label_weight),
        'learning-rate={:.2e}'.format(args.learning_rate),
        'temperature={:.2e}'.format(args.temperature),
        'zero-mean-logits={}'.format(args.zero_mean_logits)
    ])

    runpath = os.path.join(args.savepath, runname)
    print("Output path is {}".format(runpath))
    print("Teacher should be located at {}".format(args.teacherpath))
    make_savepath_if_necessary(args.savepath)
    remove_savepath_if_necessary(runpath)
    make_savepath_if_necessary(runpath)

    graph = tf.Graph()
    sess = tf.Session(graph=graph)
    with graph.as_default():
        print("Restoring teacher.")
        teacher = RestoredTeacher(
            sess, args.teacherpath, load_mnist()
        )

        learner = Learner(
            sess,
            teacher,
            runpath,
            n_hidden=[args.n_hidden]*2,
            temperature=args.temperature,
            labels_weight=args.label_weight,
            learning_rate=args.learning_rate,
            zero_mean_logits=args.zero_mean_logits
        )

        learner.train(
            n_training_steps=30000,
            n_batch=50,
            train_summary_interval=100,
            test_summary_interval=1000
        )