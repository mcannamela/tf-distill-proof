#!/usr/bin/env bash
if [[ ! -d ../tf-core ]]; then
    echo "First clone tf-core into the same parent as this repo."
    exit 1
else
    cd ../tf-core
    ./build_env.sh
fi