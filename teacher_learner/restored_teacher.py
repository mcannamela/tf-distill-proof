import tensorflow as tf

from teacher_learner.base_teacher import BaseTeacher


class RestoredTeacher(BaseTeacher):
    """A Teacher loaded from a checkpoint, with critical ops and tensors bound
    
    We load from a checkpoint made by e.g. Teacher, and we recover the things we 
    need by name. 
    
    Once those are in place, it can be trained or its outputs consulted just as 
    one would expect. 
    
    """

    def __init__(self, session, savepath, mnist):
        chkpt_file = tf.train.latest_checkpoint(savepath)
        print("Latest checkpoint is {}".format(chkpt_file))

        print("import meta graph")
        saver = tf.train.import_meta_graph(chkpt_file + '.meta')

        print("restore session")
        saver.restore(session, chkpt_file)

        super(RestoredTeacher, self).__init__(session, savepath, mnist)

    def _init_placeholders(self):
        self.input_pl = self._get_tensor_by_name(self.INPUT_PL_NAME)
        self.one_hot_labels_pl = self._get_tensor_by_name(self.LABELS_PL_NAME)
        self.is_training_pl = self._get_tensor_by_name(self.IS_TRAINING_PL_NAME)

    def _init_model(self):
        self.output_logits = self._get_tensor_by_name(self.OUTPUT_LOGITS_NAME)
        self.output_softmax = self._get_tensor_by_name(self.OUTPUT_SOFTMAX_NAME)

    def _init_loss_and_accuracy(self):
        self.xentropy = self._get_tensor_by_name(self.XENTROPY_LOSS_NAME)
        self.accuracy_train = self._get_tensor_by_name(self.ACCURACY_TRAIN_NAME)
        self.accuracy_test = self._get_tensor_by_name(self.ACCURACY_TEST_NAME)

    def _init_train_step(self):
        self.train_step = self._get_op_by_name(self.TRAIN_STEP_NAME)

    def _init_summaries(self):
        self.summ_acc_train = self._get_tensor_by_name(self.SUMM_TRAIN_ACC_NAME)
        self.summ_acc_test = self._get_tensor_by_name(self.SUMM_TEST_ACC_NAME)

    def _get_op_by_name(self, name):
        return self._session.graph.get_operation_by_name(name)

    def _get_tensor_by_name(self, name, idx=0):
        return self._session.graph.get_tensor_by_name(':'.join([name, '{}'.format(idx)]))