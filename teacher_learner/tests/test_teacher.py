import tempfile
from unittest import TestCase

import tensorflow as tf

from mnist_data import load_mnist
from teacher_learner.teacher import Teacher
from teacher_learner.restored_teacher import RestoredTeacher

#TODO: these are pretty long at the moment, would be good to get the times down
class TeacherTestCase(tf.test.TestCase, TestCase):

    def setUp(self):
        self._savepath = tempfile.mkdtemp("_teacher_model_test")

        with self.test_session() as sess:
            self.teacher = Teacher(sess, self._savepath, load_mnist())

    def test_it_trains(self):
        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            test_acc_initial = sess.run(self.teacher.accuracy_test, feed_dict=self.teacher.test_feed_dict)

            self.teacher.train(
                n_training_steps=100,
                train_summary_interval=70,
                test_summary_interval=80
            )

            test_acc_final = sess.run(self.teacher.accuracy_test, feed_dict=self.teacher.test_feed_dict)

            self.assertGreater(test_acc_final - test_acc_initial, .2)

    def test_restored_teacher_has_same_outputs(self):
        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())

            self.teacher.train(
                n_training_steps=10,
                train_summary_interval=10,
                test_summary_interval=10
            )

            exp_outputs = sess.run(self.teacher.output_logits, feed_dict=self.teacher.test_feed_dict)

        graph = tf.Graph()
        sess = tf.Session(graph=graph)

        tfd = self.teacher.test_feed_dict
        with graph.as_default():
            restored_teacher = RestoredTeacher(sess, self._savepath, load_mnist())
            feed_dict = {
                restored_teacher.input_pl: tfd[self.teacher.input_pl],
                restored_teacher.one_hot_labels_pl: tfd[self.teacher.one_hot_labels_pl],
                restored_teacher.is_training_pl: tfd[self.teacher.is_training_pl],
            }

            outputs = sess.run(restored_teacher.output_logits, feed_dict=feed_dict)

        self.assertAllClose(exp_outputs, outputs)

    def test_restored_teacher_can_train(self):
        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())

            self.teacher.train(
                n_training_steps=10,
                train_summary_interval=10,
                test_summary_interval=10
            )

            test_acc_saved = sess.run(self.teacher.accuracy_test, feed_dict=self.teacher.test_feed_dict)

        graph = tf.Graph()
        sess = tf.Session(graph=graph)
        with graph.as_default():
            restored_teacher = RestoredTeacher(sess, self._savepath, load_mnist())

            test_acc_restored = sess.run(restored_teacher.accuracy_test, feed_dict=restored_teacher.test_feed_dict)

            self.assertAlmostEqual(test_acc_saved, test_acc_restored)

            restored_teacher.train(
                n_training_steps=100,
                train_summary_interval=100,
                test_summary_interval=100
            )

            test_acc_final = sess.run(restored_teacher.accuracy_test, feed_dict=restored_teacher.test_feed_dict)

        self.assertGreater(test_acc_final - test_acc_restored, .1)

    def tearDown(self):
        tf.gfile.DeleteRecursively(self._savepath)