import tempfile
from unittest import TestCase

import tensorflow as tf

from mnist_data import load_mnist
from teacher_learner.learner import Learner
from teacher_learner.teacher import Teacher


class LearnerTestCase(tf.test.TestCase, TestCase):

    def setUp(self):
        self._savepath_teacher = tempfile.mkdtemp("_learner_model_test_teacher_dir")
        self._savepath = tempfile.mkdtemp("_learner_model_test")

        with self.test_session() as sess:
            self.teacher = Teacher(sess, self._savepath_teacher, load_mnist())
            self.learner = Learner(sess, self.teacher, self._savepath, labels_weight=.9)

    def test_it_trains(self):
        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            self.teacher.train(
                n_training_steps=200,
                train_summary_interval=200,
                test_summary_interval=200
            )

            test_acc_initial = sess.run(self.learner.accuracy_test, feed_dict=self.learner.test_feed_dict)

            self.learner.train(
                n_training_steps=200,
                train_summary_interval=200,
                test_summary_interval=200
            )

            test_acc_final = sess.run(self.learner.accuracy_test, feed_dict=self.learner.test_feed_dict)

            self.assertGreater(test_acc_final - test_acc_initial, .2)

    def tearDown(self):
        tf.gfile.DeleteRecursively(self._savepath)