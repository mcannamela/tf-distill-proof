import os

import tensorflow as tf

from teacher_learner.abstract_teacher import AbstractTeacher


class BaseTeacher(AbstractTeacher):
    """Template for construction and partial implementation for Teacher's
    
    The procedure for training and testing accuracy, and some of the saving and summarizing boilerplate 
    will be the same for some different kinds of teacher, so we implement them here. 
    
    """

    def __init__(self, session, savepath, mnist):
        """
        
        :param session: the tensorflow session
        :param savepath: Logs and summaries written to this path.
        :param mnist: The mnist data as provided by the tensorflow tutorials
        """
        super(BaseTeacher, self).__init__(session, savepath, mnist)
        self._init_placeholders()
        self._init_model()
        self._init_loss_and_accuracy()
        self._init_train_step()
        self._init_summaries()
        self._init_summary_writer_and_saver()
        self._init_feed_dicts()

    def train(self,
              n_training_steps=30000,
              n_batch=50,
              train_summary_interval=100,
              test_summary_interval=1000
              ):

        self._session.run(tf.global_variables_initializer())
        self._save(0)

        for i in range(n_training_steps):
            self._do_training_step(i, n_batch, test_summary_interval, train_summary_interval)

        self._summarize_testing(n_training_steps)

    def get_next_training_batch(self, n_batch):
        batch = self._mnist.train.next_batch(n_batch)
        return batch

    def get_test_batch(self):
        batch = (self._mnist.test.images, self._mnist.test.labels)
        return batch

    def _init_placeholders(self):
        raise NotImplementedError()

    def _init_model(self):
        raise NotImplementedError()

    def _init_loss_and_accuracy(self):
        raise NotImplementedError()

    def _init_train_step(self):
        raise NotImplementedError()

    def _init_summaries(self):
        raise NotImplementedError()

    def _init_feed_dicts(self):
        self.test_feed_dict = {
            self.input_pl: self._mnist.test.images, self.one_hot_labels_pl: self._mnist.test.labels,
            self.is_training_pl: False
        }

        self.last_feed_dict = {}

    def _init_summary_writer_and_saver(self):
        self.summary_writer = tf.summary.FileWriter(self._savepath, self._session.graph)
        self.saver = tf.train.Saver()

    def _do_training_step(self, i, n_batch, test_summary_interval, train_summary_interval):
        batch = self.get_next_training_batch(n_batch)
        self.last_feed_dict = {self.input_pl: batch[0], self.one_hot_labels_pl: batch[1], self.is_training_pl: True}
        self._session.run(self.train_step, feed_dict=self.last_feed_dict)
        if i % train_summary_interval == 0:
            self._summarize_training(i)
        if i % test_summary_interval == 0:
            self._summarize_testing(i)

    def _summarize_testing(self, step):
        this_test_acc, summ = self._session.run([self.accuracy_test, self.summ_acc_test], feed_dict=self.test_feed_dict)
        self._write_summary(summ, step)
        self._save(step)
        print("test accuracy %g" % this_test_acc)

    def _summarize_training(self, step):
        self.last_feed_dict[self.is_training_pl] = False
        this_train_acc, summ = self._session.run([self.accuracy_train, self.summ_acc_train], feed_dict=self.last_feed_dict)
        self._write_summary(summ, step)
        print("step %d, training accuracy %g" % (step, this_train_acc))

    def _write_summary(self, summ, step):
        self.summary_writer.add_summary(summ, step)
        self.summary_writer.flush()

    def _save(self, step):
        self.saver.save(self._session, os.path.join(self._savepath, "model.ckpt"), global_step=step)