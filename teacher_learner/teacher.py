import numpy as np
import tensorflow as tf
from tensorflow.contrib import layers

from constants import MNIST
from teacher_learner.base_teacher import BaseTeacher


class Teacher(BaseTeacher):
    """A convolution-maxpool network with softmax cross-entropy loss

        Used as the 'big model' in our distillation proof to teach the small model with its outputs.
    
    """

    def __init__(self, session, savepath, mnist, n_filters=(32, 64), kernel_sizes=(5, 5)):
        """

        :param session: the tensorflow session.
        :param mnist: Tensorflow's mnist structure, from the tutorials
        :param savepath: Checkpoints and summaries written this directory.
        :param n_filters: Number of filters in each convolution layer.
        :param kernel_sizes: Size of kernel in each convolution layer.

        """
        self.n_filters = n_filters
        self.kernel_sizes = kernel_sizes

        if not len(self.n_filters) == len(self.kernel_sizes):
            raise RuntimeError("Need a number of filters and kernel size for each conv-maxpool layer")

        super(Teacher, self).__init__(session, savepath, mnist)

    def _init_placeholders(self):
        self.input_pl = tf.placeholder(tf.float32, shape=[None, MNIST.N_PIXELS_FLAT], name=self.INPUT_PL_NAME)
        self.one_hot_labels_pl = tf.placeholder(tf.float32, shape=[None, MNIST.N_LABELS], name=self.LABELS_PL_NAME)
        self.is_training_pl = tf.placeholder(tf.bool, name=self.IS_TRAINING_PL_NAME)

    def _init_model(self):
        input_image = tf.reshape(self.input_pl, [-1, MNIST.N_PIXELS_SQUARE, MNIST.N_PIXELS_SQUARE, 1])

        this_input = input_image
        max_pools = []
        n_layers = len(self.n_filters)
        for i in range(n_layers):
            mp = self._build_conv_and_maxpool_layer(this_input, i)
            max_pools.append(mp)
            this_input = mp

        last_max_pool = max_pools[-1]

        n_elements_per_example = self._get_n_elements_per_example_for_last_max_pool(last_max_pool, n_layers)

        fc = layers.fully_connected(
            layers.flatten(last_max_pool),
            n_elements_per_example,
            activation_fn=tf.nn.relu,
            biases_initializer=tf.constant_initializer(.1)
        )
        drop = layers.dropout(
            fc,
            keep_prob=.5,
            is_training=self.is_training_pl
        )
        fully_connected = layers.fully_connected(
            layers.flatten(drop),
            10,
            activation_fn=tf.nn.relu,
            biases_initializer=tf.constant_initializer(.1)
        )

        self.output_logits = tf.identity(fully_connected, name=self.OUTPUT_LOGITS_NAME)
        self.output_softmax = tf.nn.softmax(self.output_logits, name=self.OUTPUT_SOFTMAX_NAME)

    def _init_loss_and_accuracy(self):
        xentropy = tf.losses.softmax_cross_entropy(self.one_hot_labels_pl, self.output_logits)
        self.xentropy = tf.identity(xentropy, name=self.XENTROPY_LOSS_NAME)

        true_label = tf.argmax(self.one_hot_labels_pl, axis=1)
        predicted_label = tf.argmax(self.output_logits, axis=1)
        correct_prediction = tf.equal(predicted_label, true_label)

        self.accuracy_train = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name=self.ACCURACY_TRAIN_NAME)
        self.accuracy_test = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name=self.ACCURACY_TEST_NAME)

    def _init_train_step(self):
        optimizer = tf.train.AdamOptimizer(learning_rate=2e-5, beta1=0.99, beta2=0.9999, epsilon=1e-7)
        self.train_step = optimizer.minimize(self.xentropy, name=self.TRAIN_STEP_NAME)

    def _init_summaries(self):
        self.summ_acc_train = tf.summary.scalar(self.SUMM_TRAIN_ACC_NAME, self.accuracy_train)
        self.summ_acc_test = tf.summary.scalar(self.SUMM_TEST_ACC_NAME, self.accuracy_test)

    def _build_conv_and_maxpool_layer(self, input, layer_idx):
        conv1 = layers.convolution2d(
            input,
            self.n_filters[layer_idx],
            self.kernel_sizes[layer_idx],
            activation_fn=tf.nn.relu,
            biases_initializer=tf.constant_initializer(.1),
        )
        max_pool_1 = layers.max_pool2d(conv1, 2, 2)
        return max_pool_1

    def _get_n_elements_per_example_for_last_max_pool(self, last_max_pool, n_layers):
        n_elements_per_example_blind = self.n_filters[-1] * (MNIST.N_PIXELS_SQUARE // (2 ** n_layers)) ** 2
        n_elements_per_example = np.prod(last_max_pool.get_shape().as_list()[1:])
        if not n_elements_per_example == n_elements_per_example_blind:
            raise RuntimeError(
                "Unexpected dimension for last max pool output: {} != {}".format(n_elements_per_example_blind,
                                                                                 n_elements_per_example))
        return n_elements_per_example
