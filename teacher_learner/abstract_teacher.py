class AbstractTeacher(object):
    """Define names of tensors and members for important ops
    
    Teachers should have all the properties declared in the init here, 
    and must be sure to name their tensors with the names provided, so that
    we can recover particular ops or tensors after loading. 
    
    """
    INPUT_PL_NAME = 'input_pl'
    LABELS_PL_NAME = 'labels_pl'
    IS_TRAINING_PL_NAME = 'is_training_pl'

    OUTPUT_LOGITS_NAME = 'output_logits'
    OUTPUT_SOFTMAX_NAME = 'output_sigmoid'
    XENTROPY_LOSS_NAME = 'xentropy_loss'

    ACCURACY_TRAIN_NAME = 'accuracy_train'
    ACCURACY_TEST_NAME = 'accuracy_test'

    TRAIN_STEP_NAME = 'train_step'

    SUMM_TRAIN_ACC_NAME = 'summ_acc_train'
    SUMM_TEST_ACC_NAME = 'summ_acc_test'


    def __init__(self, session, savepath, mnist):
        self._session = session
        self._savepath = savepath
        self._mnist = mnist

        self.input_pl = None
        self.one_hot_labels_pl = None
        self.is_training_pl = None

        self.output_logits = None
        self.output_softmax = None

        self.accuracy_train = None
        self.accuracy_test = None

        self.train_step = None
        self.last_feed_dict = None
        self.test_feed_dict = None

        self.summ_acc_train = None
        self.summ_acc_test = None
        self.summary_writer = None

        self.saver = None

    def train(self):
        raise NotImplementedError()

    def get_next_training_batch(self, n_batch):
        raise NotImplementedError()

    def get_test_batch(self):
        raise NotImplementedError()