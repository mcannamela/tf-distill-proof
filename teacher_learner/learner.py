import os

import tensorflow as tf
from tensorflow.contrib import layers

from constants import MNIST

#TODO: Learner has too much copy-pasta from BaseTeacher. Not sure how, but that duplication should be eliminated
class Learner(object):
    """A plain neural net for distilling something fancier
    
    Given a Teacher instance, the Learner sets up losses and training procedures
    that allow it to learn a blend of the Teacher's outputs and labels. 
    
    """

    OUTPUT_LOGITS_TRAIN_NAME = 'output_logits_train_learner'
    OUTPUT_LOGITS_TEST_NAME = 'output_logits_test_learner'
    TEACHER_LOGITS_NAME = 'teacher_logits'
    TEACHER_SOFTMAX_NAME = 'teacher_softmax'

    ACCURACY_TRAIN_NAME = 'accuracy_train_learner'
    ACCURACY_TEST_NAME = 'accuracy_test_learner'
    ACCURACY_TEST_RELATIVE_NAME = 'accuracy_test_relative'

    TRAIN_STEP_NAME = 'train_step_learner'

    SUMM_TRAIN_ACC_NAME = 'summ_acc_train_learner'
    SUMM_TEST_ACC_NAME = 'summ_acc_test_learner'
    SUMM_TEST_ACC_REL_NAME = 'summ_acc_test_rel_learner'

    def __init__(self,
                 session, teacher, savepath,
                 n_hidden=(300, 300),
                 temperature=1.0,
                 labels_weight=.1,
                 learning_rate=1e-4,
                 zero_mean_logits=False):
        """
        
        :param session: the tensorflow session
        :param teacher: an instance of AbstractTeacher
        :param savepath: Checkpoints and summaries written here
        :param n_hidden: A list of ints giving the number of units in each hidden layer of the network. 
        :param temperature: Hinton's temperature for the transfer process; usual softmax assumes T=1.0. In the high 
            temperature limit, every class is equiprobable no matter what the logits are.
        :param labels_weight: A float between 0 and 1 giving the weight on the data labels; the weight on the teacher's 
            outputs is (1.0 - labels_weight)
        :param learning_rate: 
        """
        self._session = session
        self._teacher = teacher
        self._savepath = savepath
        self.n_hidden = n_hidden
        self.temperature = temperature
        self.labels_weight = labels_weight
        self.learning_rate = learning_rate
        self.zero_mean_logits = zero_mean_logits

        if self.labels_weight < 0.0 or self.labels_weight > 1.0:
            raise RuntimeError("Keep labels_weight between 0 and 1: {}".format(self.labels_weight))

        self._init_placeholders()
        self._init_model()
        self._init_loss_and_accuracy()
        self._init_train_step()
        self._init_summaries()
        self._init_summary_writer_and_saver()
        self._init_feed_dicts()

    def train(self,
              n_training_steps=30000,
              n_batch=50,
              train_summary_interval=100,
              test_summary_interval=1000
              ):

        self._session.run(tf.global_variables_initializer())
        self._save(0)

        for i in range(n_training_steps):
            self._do_training_step(i, n_batch, test_summary_interval, train_summary_interval)

        self._summarize_testing(n_training_steps)

    def _init_placeholders(self):
        self.input_pl = self._teacher.input_pl
        self.one_hot_labels_pl = self._teacher.one_hot_labels_pl
        self.is_training_pl = self._teacher.is_training_pl

    def _init_model(self):
        this_input = self.input_pl
        hidden = []
        with tf.name_scope('learner') as scope:
            for n in self.n_hidden:
                h = layers.fully_connected(
                    this_input,
                    n,
                    activation_fn=tf.nn.relu,
                    biases_initializer=tf.constant_initializer(.1),
                )
                hidden.append(h)
                this_input = h

            output = layers.fully_connected(
                    hidden[-1], MNIST.N_LABELS,
                    activation_fn=tf.nn.relu,
                    biases_initializer=tf.constant_initializer(.1),
                )

            self.output_logits_train = tf.identity(output/self.temperature, name=self.OUTPUT_LOGITS_TRAIN_NAME)
            self.output_logits_test = tf.identity(output, name=self.OUTPUT_LOGITS_TEST_NAME)

            raw_teacher_logits = self._teacher.output_logits
            if self.zero_mean_logits:
                teacher_logits = (raw_teacher_logits - tf.reduce_mean(raw_teacher_logits, axis=1, keep_dims=True))
            else:
                teacher_logits = raw_teacher_logits

            self.teacher_logits = tf.identity(teacher_logits/self.temperature, name=self.TEACHER_LOGITS_NAME)
            self.teacher_softmax = tf.nn.softmax(self.teacher_logits, name=self.TEACHER_SOFTMAX_NAME)

    def _init_loss_and_accuracy(self):
        self.xentropy_teacher = tf.losses.softmax_cross_entropy(
            self.teacher_softmax,
            self.output_logits_train,
            weights=self.temperature**2
        )

        self.xentropy_labels = tf.losses.softmax_cross_entropy(
            self.one_hot_labels_pl,
            self.output_logits_train,
            weights=self.temperature**2
        )

        self.loss = (1.0 - self.labels_weight)*self.xentropy_teacher + self.labels_weight*self.xentropy_labels

        true_label = tf.argmax(self.one_hot_labels_pl, axis=1)
        predicted_label = tf.argmax(self.output_logits_test, axis=1)
        correct_prediction = tf.equal(predicted_label, true_label)

        self.accuracy_train = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name=self.ACCURACY_TRAIN_NAME)
        self.accuracy_test = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name=self.ACCURACY_TEST_NAME)
        self.accuracy_test_relative = tf.identity(
            self.accuracy_test/self._teacher.accuracy_test,
            name=self.ACCURACY_TEST_RELATIVE_NAME
        )

    def _init_train_step(self):
        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate, beta1=0.99, beta2=0.9999, epsilon=1e-7)
        self.train_step = optimizer.minimize(self.loss, name=self.TRAIN_STEP_NAME)

    def _init_summaries(self):
        self.summ_acc_train = tf.summary.scalar(self.SUMM_TRAIN_ACC_NAME, self.accuracy_train)
        self.summ_acc_test = tf.summary.scalar(self.SUMM_TEST_ACC_NAME, self.accuracy_test)
        self.summ_acc_test_relative = tf.summary.scalar(
            self.SUMM_TEST_ACC_REL_NAME,
            self.accuracy_test_relative
        )

    def _init_feed_dicts(self):
        test_batch = self._teacher.get_test_batch()
        self.test_feed_dict = {
            self.input_pl: test_batch[0], self.one_hot_labels_pl: test_batch[1],
            self.is_training_pl: False
        }

        self.last_feed_dict = {}

    def _init_summary_writer_and_saver(self):
        self.summary_writer = tf.summary.FileWriter(self._savepath, self._session.graph)
        self.saver = tf.train.Saver()

    def _do_training_step(self, i, n_batch, test_summary_interval, train_summary_interval):
        batch = self._teacher.get_next_training_batch(n_batch)
        self.last_feed_dict = {self.input_pl: batch[0], self.one_hot_labels_pl: batch[1], self.is_training_pl: True}
        self._session.run(self.train_step, feed_dict=self.last_feed_dict)
        if i % train_summary_interval == 0:
            self._summarize_training(i)
        if i % test_summary_interval == 0:
            self._summarize_testing(i)

    def _summarize_testing(self, step):
        this_test_acc, summ, summ_rel = self._session.run(
            [self.accuracy_test, self.summ_acc_test, self.summ_acc_test_relative],
            feed_dict=self.test_feed_dict
        )
        self._write_summary(summ, step)
        self._write_summary(summ_rel, step)
        self._save(step)
        print("test accuracy %g" % this_test_acc)

    def _summarize_training(self, step):
        self.last_feed_dict[self.is_training_pl] = False
        this_train_acc, summ = self._session.run([self.accuracy_train, self.summ_acc_train], feed_dict=self.last_feed_dict)
        self._write_summary(summ, step)
        print("step %d, training accuracy %g" % (step, this_train_acc))

    def _write_summary(self, summ, step):
        self.summary_writer.add_summary(summ, step)
        self.summary_writer.flush()

    def _save(self, step):
        self.saver.save(self._session, os.path.join(self._savepath, "model.ckpt"), global_step=step)



