from argument_parser import get_parser
import tensorflow as tf

from mnist_data import load_mnist
from teacher_learner import Learner, Teacher
from matplotlib import pyplot as plt
import numpy as np
import os
import pickle

parser = get_parser()
parser.add_argument('--from-pickle', action='store_true', default=False)

PICKLENAME = os.path.join(os.path.expanduser('~'), 'mnist_distill_acc.pkl')


class PartiallyRestoredLearner(object):

    def __init__(self, session, savepath):
        self._session = session

        chkpt_file = tf.train.latest_checkpoint(savepath)
        print("Latest checkpoint is {}".format(chkpt_file))

        print("import meta graph")
        saver = tf.train.import_meta_graph(chkpt_file + '.meta')

        print("restore session")
        saver.restore(session, chkpt_file)
        self._init_placeholders()
        self._init_accuracy()

    def _init_placeholders(self):
        self.input_pl = self._get_tensor_by_name(Teacher.INPUT_PL_NAME)
        self.one_hot_labels_pl = self._get_tensor_by_name(Teacher.LABELS_PL_NAME)
        self.is_training_pl = self._get_tensor_by_name(Teacher.IS_TRAINING_PL_NAME)

    def _init_accuracy(self):
        self.accuracy_test = self._get_tensor_by_name(Learner.ACCURACY_TEST_NAME)

    def _get_tensor_by_name(self, name, idx=0):
        return self._session.graph.get_tensor_by_name(':'.join([name, '{}'.format(idx)]))


class RunDims(object):
    @classmethod
    def build_from_runpath_dict(cls, d):
        return cls(d['n-hidden'], d['label-weight'], d['learning-rate'], d['temperature'])

    def __init__(self, n_hidden, label_weight, learning_rate, temperature):
        self.n_hidden = int(n_hidden)
        self.label_weight = float(label_weight)
        self.learning_rate = float(learning_rate)
        self.temperature = float(temperature)

    def __str__(self):
        return "<{}: n={}, w={}, r={}, t={}>".format(
            self.__class__.__name__,
            self.n_hidden,
            self.label_weight,
            self.learning_rate,
            self.temperature
        )


class AccPoint(object):
    def __init__(self, run_dims, accuracy):
        self.run_dims = run_dims
        self.accuracy = accuracy

    def __str__(self):
        return "<{}: d={}, a={}>".format(self.__class__.__name__, self.run_dims, self.accuracy)


class Loader(object):
    def __init__(self, savepath):
        self.mnist = load_mnist()
        self.test_batch = (self.mnist.test.images, self.mnist.test.labels)
        self.savepath = savepath

    def load(self, runpath):
        print("load {}".format(runpath))
        run_dims = self.parse_path(runpath)

        graph = tf.Graph()
        sess = tf.Session(graph=graph)
        with graph.as_default():
            learner = PartiallyRestoredLearner(sess, os.path.join(self.savepath, runpath))

        feed_dict = {
            learner.input_pl: self.test_batch[0],
            learner.one_hot_labels_pl: self.test_batch[1],
            learner.is_training_pl: False
        }

        acc = sess.run(learner.accuracy_test, feed_dict=feed_dict)
        print("    acc = {}".format(acc))

        return AccPoint(run_dims, acc)

    def parse_path(self, runpath):
        elements = runpath.split('_')
        pairs = [el.split('=') for el in elements]
        d = {p[0]: p[1] for p in pairs}
        rd = RunDims.build_from_runpath_dict(d)
        return rd


if __name__ == "__main__":
    args = parser.parse_args()

    if not args.from_pickle or not os.path.isfile(PICKLENAME):
        loader = Loader(args.savepath)
        points = map(loader.load, os.listdir(args.savepath))

        # remove outlier due to bad checkpoint
        points = [p for p in points if p.accuracy > .5]
        with open(PICKLENAME, 'wb') as f:
            pickle.dump(points, f)

    else:
        with open(PICKLENAME, 'rb') as f:
            points = pickle.load(f)

    for p in points:
        print(p)

    def extract_t_a(p):
        return [p.run_dims.temperature + .1*np.random.randn(), p.accuracy]

    control_200 = np.array([extract_t_a(p) for p in points if p.run_dims.n_hidden == 200 and p.run_dims.label_weight > .99]).T
    control_50 = np.array([extract_t_a(p) for p in points if p.run_dims.n_hidden == 50 and p.run_dims.label_weight > .99]).T

    learn_200 = np.array([extract_t_a(p) for p in points if p.run_dims.n_hidden == 200 and .09 < p.run_dims.label_weight < .11]).T
    learn_50 = np.array([extract_t_a(p) for p in points if p.run_dims.n_hidden == 50 and .09 < p.run_dims.label_weight < .11]).T

    plt.figure()
    plt.plot(learn_50[0], learn_50[1], 'bo', label='n=50, learn', alpha=.75)
    plt.plot(learn_200[0], learn_200[1], 'b^', label='n=200, learn', alpha=.75)
    plt.plot(control_50[0], control_50[1], 'ks', label='n=50, ctrl', alpha=.75)
    plt.plot(control_200[0], control_200[1], 'kD', label='n=200, ctrl', alpha=.75)
    plt.legend(loc='lower right')
    plt.xlabel("Temperature (jittered)")
    plt.ylabel("Accuracy")
    plt.title("Accuracy of Distilled Models (teacher accuracy = .9912")

    plt.show()